from flask import Flask
from flask_restful import Api
from flask_cors import CORS
from constants import DEVELOPMENT, STAGE, PRODUCTION
from os import environ
from database.db import initialize_db

app = Flask(__name__)

app.config.from_object("config.DevConfig")


@app.route("/")
def is_running():
    return "The api is up and running!"


CORS(app)
api = Api(app)
initialize_db(app)

from resources.routes import initialize_routes
initialize_routes(api)