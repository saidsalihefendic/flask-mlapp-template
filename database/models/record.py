from database.db import db
import datetime

class PredictionHistoryRecord(db.Document):
    data = db.StringField(required=True)
    predicted_value = db.StringField(required=True)
    predicted_at = db.DateTimeField(default=datetime.datetime.utcnow)