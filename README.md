# Machine Learning App Template

This repository provides some basic setup for a finished Machine Learning Project and integrating the results in Flask API project powered by MongoDB instance on Mongo Atlas and automatically deploy to Heroku (which represents Stage environment).

The project uses DevOps practices and deploys to Heroku App for any merge request to dev and master branch and you will find the tests folder where you can define your own unit tests for the project of your need.

The MLApp Template uses Decision Tree Classifier with data preparation, design and ML training with the dataset from this article:

https://medium.com/codex/credit-card-fraud-detection-with-machine-learning-in-python-ac7281991d87

To use the repository in full capabilities, you will need to setup your environment variables on your local machine, Gitlab repository and Heroku app:

- HEROKU_API_KEY - your Heroku account API Key (Gitlab Repository)
- HEROKU_APP_NAME - your Heroku app name (Gitlab Repository)
- MONGODB_HOST - your Mongo Atlas connection string to your Cluster (Local Machine, Gitlab Repository, Heroku)

In Gitlab, under Settings->CI/CD, you will find Variable section, add these variables and set all Flags (Protected and Mask) to 'unchecked', if your dev and master branches are not protected.

In Heroku App, under Settings->Config vars insert MONGODB_HOST variable with the connection string to the Mongo Atlas Cluster (Note: For first time use, let the Cluster be accessible by anyone, then later you can list IP Addresses that can connect to that cluster).

To run the app locally (after setting the MongoDB Host Environment variable), the recommended procedure is to create a new virtual environment, install Python packages and run the Flask project. In this example using miniconda:

```
conda create -n mlapp python=3.8
conda activate mlapp
pip install -r requirements.txt
python run.py
```

Then test the POST request to the localhost:5000/api/predict with JSON body (Postman is recommended):

```
{"V1": -2.3122265423263,
 "V2": 1.95199201064158,
 "V3": -1.60985073229769,
 "V4": 3.9979055875468,
 "V5": -0.522187864667764,
 "V6": -1.42654531920595,
 "V7": -2.53738730624579,
 "V8": 1.39165724829804,
 "V9": -2.77008927719433,
 "V10": -2.77227214465915,
 "V11": 3.20203320709635,
 "V12": -2.89990738849473,
 "V13": -0.595221881324605,
 "V14": -4.28925378244217,
 "V15": 0.389724120274487,
 "V16": -1.14074717980657,
 "V17": -2.83005567450437,
 "V18": -0.0168224681808257,
 "V19": 0.416955705037907,
 "V20": 0.126910559061474,
 "V21": 0.517232370861764,
 "V22": -0.0350493686052974,
 "V23": -0.465211076182388,
 "V24": 0.320198198514526,
 "V25": 0.0445191674731724,
 "V26": 0.177839798284401,
 "V27": 0.261145002567677,
 "V28": -0.143275874698919,
 "Amount": 0.0
}
```

which will return the data that is sent in string JSON and predicted value. In the meantime, you should check your Mongo Atlas Collections on your Cluster and check prediction_record_history, where you will find the tested prediction and saved in the collection.

You should also test the deployed project to your Heroku App with the same parameters as local machine test, just change the localhost to the URL of your Heroku app.

This project uses Python 3.8.

You will find the project configuration in config.py, where you can customize your own configs for multiple environments, such as production, another staging environment and development environment, but this project currently has only DevConfig for development configuration.

## Next Steps
The next steps with this project is to develop a Data Pipeline for much more complicated data preprocessing and define the Document models according to the dataset where Machine Learning Techniques are applied, which will be updated in the near future.