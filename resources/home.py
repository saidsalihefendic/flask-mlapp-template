from flask_restful import Resource


class HomeApi(Resource):
    def get(self):
        return {
            "content": "This is the MLApp!",
            "author": "Neko ime i prezime"
        }, 200
