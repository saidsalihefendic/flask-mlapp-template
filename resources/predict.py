import os
from flask import request
from flask_restful import Resource
from database.models.record import PredictionHistoryRecord
from random import randint, random
from sklearn.preprocessing import StandardScaler
import pandas as pd
import pickle
import json

sc = StandardScaler()

filehandler = open(os.path.join("ml_models", "DTClassifier.obj"), 'rb')
tree_model = pickle.load(filehandler)

class PredictApi(Resource):
    def post(self):
        body = request.get_json()

        df = pd.DataFrame.from_dict([body])
        amount = df['Amount'].values
        df['Amount'] = sc.fit_transform(amount.reshape(-1, 1))

        # load model
        prediction = tree_model.predict(df.values)[0]

        predicted_value = random()
        
        payload = {
            "data": json.dumps(body),
            "predicted_value": str(prediction)
        }

        prediction_history_record = PredictionHistoryRecord(**payload)

        prediction_history_record.save()

        return payload, 200
