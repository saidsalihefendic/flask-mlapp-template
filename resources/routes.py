from .home import HomeApi
from .predict import PredictApi

def initialize_routes(api):
    api.add_resource(HomeApi, "/api/home")
    api.add_resource(PredictApi, "/api/predict")
